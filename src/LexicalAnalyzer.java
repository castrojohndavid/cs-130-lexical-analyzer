/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package lexicalanalyzer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author jd
 */
public class LexicalAnalyzer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String fileName = "";
        try{
            fileName = args[0];
        }
        catch(Exception ex){
            System.out.println("please input a file name");
            System.exit(0);
        }
        
        BufferedReader br = null;
        FileReader fr = null;
        
        
        String currLine = "";
        
        try{
            fr = new FileReader(fileName);
            br = new BufferedReader(fr);
            
            String sCurrentLine;
            
            while((sCurrentLine = br.readLine())!=null){
		currLine += sCurrentLine+" ";
            }
        }
        catch (Exception ex){
            System.out.println("no file");
        }
        
        String program = currLine+"E";
        //String program = "<table>";
	//program = program.replaceAll("\\s","");
        ArrayList<String> charQueue = new ArrayList<>();
        //System.out.println(program);
        
        
        
        HashMap<Integer, String> finalStateTokenMap = new HashMap();
        
        finalStateTokenMap.put(1, "LTHAN");
        finalStateTokenMap.put(2, "TAGIDENT");
        finalStateTokenMap.put(3, "NUMBER");
        finalStateTokenMap.put(4, "***lexical error: badly formed number");
        finalStateTokenMap.put(5, "***lexical error: badly formed number");
        finalStateTokenMap.put(6, "***lexical error: illegal character");
        finalStateTokenMap.put(7, "IDENT");
        finalStateTokenMap.put(8, "EXPONENT");
        finalStateTokenMap.put(9, "GTHAN");
        finalStateTokenMap.put(10, "ENDTAGHEAD");
        finalStateTokenMap.put(11, "LTHAN");
        finalStateTokenMap.put(13, "PLUS");
        finalStateTokenMap.put(14, "MINUS");
        finalStateTokenMap.put(15, "MULT");
        finalStateTokenMap.put(16, "DIVIDE");
        finalStateTokenMap.put(17, "MODULO");
        finalStateTokenMap.put(18, "LPAREN");
        finalStateTokenMap.put(19, "RPAREN");
        finalStateTokenMap.put(20, "EQUALS");
        finalStateTokenMap.put(21, "COLON");
        finalStateTokenMap.put(22, "COMMA");
        finalStateTokenMap.put(23, "SCOLON");
        finalStateTokenMap.put(24, "PERIOD");
        finalStateTokenMap.put(25, "QUOTE");
        finalStateTokenMap.put(26, "PLUS");
        
        
        
        for(int i=0; i < program.length(); i++){
            charQueue.add(program.substring(i,i+1));
        }
        

        int prevState = 0;
       
        //int indexCheck = 0;
        //System.out.println("HELLO");
        int currentState = 0;
        String tempWord = "";
        while(!charQueue.isEmpty()){
            
            String character = charQueue.get(0);
           
            //switch case will involve the state 
            if((currentState == 6
                    || currentState == 8
                    || currentState == 9 
                    || currentState == 10                    
                    || currentState == 13
                    || currentState == 14
                    || currentState == 16
                    || currentState == 17
                    || currentState == 18
                    || currentState == 19
                    || currentState == 20
                    || currentState == 21
                    || currentState == 22
                    || currentState == 23
                    || currentState == 24
                    || currentState == 25
                    || currentState == 26)
                    && prevState != 4
                    && prevState != 5){
                System.out.println(finalStateTokenMap.get(currentState)+" "+ tempWord);
                tempWord = "";
            }
              
            prevState = currentState;
           
            currentState = caseState(character, currentState);
            
                tempWord += character;
            
            
            //handles cases for identifiers and tagident
            if(prevState == 7 && currentState != 7 || prevState == 2 && currentState != 2){
                System.out.println(finalStateTokenMap.get(prevState)+" "+ tempWord.substring(0, tempWord.length()-1));
                tempWord = tempWord.substring(tempWord.length()-1);
            }
            
            //handles cases for LTHAN
            if(prevState == 1 && (currentState!=2 && currentState !=10)){
                System.out.println(finalStateTokenMap.get(prevState)+" "+ tempWord.substring(0, tempWord.length()-1));
                tempWord = tempWord.substring(tempWord.length()-1);
            }
            
            //handles numbers
            if(prevState == 3 && currentState!=3 && currentState!=4 && currentState!=5){
                System.out.println(finalStateTokenMap.get(prevState)+" "+ tempWord.substring(0, tempWord.length()-1));
                tempWord = tempWord.substring(tempWord.length()-1);
            }
            
            //handles badly formed numbers
            if((prevState == 4 || prevState == 5) && currentState!=3){
                System.out.println(finalStateTokenMap.get(prevState)+" "+ tempWord);
                tempWord = "";
            }
            
            //handles multiplication sign
            if(prevState == 15 && currentState!=8){
                System.out.println(finalStateTokenMap.get(prevState)+" "+ tempWord.substring(0, tempWord.length()-1));
                tempWord = tempWord.substring(tempWord.length()-1);
            }
            
            
            
            charQueue.remove(0);
            
            
            
        }

               
    }
    
    public static int caseState(String nextChar, int currState){
        if(isDigit(nextChar)){
            return 3;
        }
        if(currState == 1 || currState == 2){
            
        }
        if(currState == 3 && nextChar.equals(".")){
            return 4;
        }
        if(currState == 3 && nextChar.equals("e")){
            return 5;
        }
        
        if(currState == 1 && nextChar.equals("/")){
            return 10;
        }
        if(currState == 15 && nextChar.equals("*")){
            return 8;
        }
        
        switch(nextChar){
            case "<":
                return 1;
            case ">":
                return 9;
            case "+":
                return 13;
            case "-":
                return 14;
            case "*":
                return 15;
            case "/":
                return 16;
            case "%":
                return 17;
            case "(":
                return 18;
            case ")":
                return 19;
            case "=":
                return 20;
            case ":":
                return 21;
            case ",":
                return 22;
            case ";":
                return 23;
            case ".":
                return 24;
            case "'":
                return 25;
            case "\"":
                return 26;
            case " ":
                return 12;
            case "$":
                return 6;
            case "!":
                return 6;
            case "&":
                return 6;
           
        }
        if(currState != 1 && currState !=2){
            return 7;
        }
        else{
            return 2;
        }
    }
    
    public static boolean isDigit(String character){
        switch(character){
            case "0":
                return true;
            case "1":
                return true;
            case "2":
                return true;
            case "3":
                return true;
            case "4":
                return true;
            case "5":
                return true;
            case "6":
                return true;
            case "7":
                return true;
            case "8":
                return true;
            case "9":
                return true;
            default:
                return false;
        }
    }
    
}